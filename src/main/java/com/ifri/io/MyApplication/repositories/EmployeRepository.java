package com.ifri.io.MyApplication.repositories;

import com.ifri.io.MyApplication.entities.Employe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeRepository extends JpaRepository<Employe, Long> {
}
