package com.ifri.io.MyApplication.repositories;

import com.ifri.io.MyApplication.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "select * from CLIENT inner join COMPTE co on CLIENT.nom=co.NOM_CLIENT and CLIENT.PRENOM=co.PRENOM_CLIENT where co.NUMERO_AGENCE=?1",
            nativeQuery = true)
    List<Client> getClientsByAgence(String numeroAgence);

    List<Client> findClientByNomLikeAndPrenomLike(String nom, String prenom);

    @Query(value = "select * from CLIENT inner join COMPTE co on CLIENT.nom=co.NOM_CLIENT and CLIENT.PRENOM=co.PRENOM_CLIENT where co.NUMERO_BANQUE=?1" +
            " AND co.TYPE=?2", nativeQuery = true)
    List<Client> getClientsADecouvertByBanque(String numeroBanque, String typeCompte);

    @Query(value = "select * from CLIENT inner join COMPTE co on CLIENT.nom=co.NOM_CLIENT and CLIENT.PRENOM=co.PRENOM_CLIENT where co.TYPE=?1", nativeQuery = true)
    List<Client> getClientsByTypeCompte(String typeCompte);

    boolean existsByNumero(String numero);
}
