package com.ifri.io.MyApplication.repositories;

import com.ifri.io.MyApplication.entities.Compte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRepository extends JpaRepository<Compte, Long> {
}
