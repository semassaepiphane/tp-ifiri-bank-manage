package com.ifri.io.MyApplication.repositories;

import com.ifri.io.MyApplication.entities.Agence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgenceRepository extends JpaRepository<Agence, Long> {
    boolean existsByNumero(String numero);
}
