package com.ifri.io.MyApplication.repositories;

import com.ifri.io.MyApplication.entities.Banque;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BanqueRepository extends JpaRepository<Banque, Long> {
    boolean existsByNumero(String numero);
}
