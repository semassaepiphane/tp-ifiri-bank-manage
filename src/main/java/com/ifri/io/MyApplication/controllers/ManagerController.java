package com.ifri.io.MyApplication.controllers;

import com.ifri.io.MyApplication.entities.Client;
import com.ifri.io.MyApplication.entities.Compte;
import com.ifri.io.MyApplication.entities.enums.TypeCompte;
import com.ifri.io.MyApplication.repositories.AgenceRepository;
import com.ifri.io.MyApplication.repositories.BanqueRepository;
import com.ifri.io.MyApplication.repositories.ClientRepository;
import com.ifri.io.MyApplication.repositories.CompteRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.PostConstruct;
import java.util.Date;

@Controller
@RequestMapping("/clients")
public class ManagerController {
    private long currentClientNumero = 0;
    private long currentCompteNumero = 0;

    private final ClientRepository clientRepository;
    private final CompteRepository compteRepository;
    private final AgenceRepository agenceRepository;
    private final BanqueRepository banqueRepository;

    public ManagerController(ClientRepository clientRepository, CompteRepository compteRepository, AgenceRepository agenceRepository, BanqueRepository banqueRepository) {
        this.clientRepository = clientRepository;
        this.compteRepository = compteRepository;
        this.agenceRepository = agenceRepository;
        this.banqueRepository = banqueRepository;
    }

    @PostConstruct
    public void init(){
        currentClientNumero = clientRepository.count() + 1;
        currentCompteNumero = compteRepository.count() + 1;
    }

    @GetMapping("/listClientsByAgency")
    public String getClientsByAgence(Model model) {
        model.addAttribute("clients", clientRepository.findAll());
        return "list-clients-agency";
    }

    @PostMapping("/listClientsByAgency")
    public RedirectView searchClientsByAgence(@ModelAttribute("agenceNumber") String numeroAgence, RedirectAttributes redirectAttributes) {
        final RedirectView redirectView = new RedirectView("/clients/listClientsByAgency", true);
        redirectAttributes.addFlashAttribute("clients", clientRepository.getClientsByAgence(numeroAgence));
        return redirectView;
    }

    @GetMapping("/addClient")
    public String addClientView(Model model) {
        model.addAttribute("client", new Client());
        return "add-client";
    }

    @PostMapping("/addClient")
    public RedirectView addClient(@ModelAttribute("client") Client client, @ModelAttribute("numeroAgence") String numeroAgence,
                                  @ModelAttribute("numeroBanque") String numeroBanque,
                                  RedirectAttributes redirectAttributes) {
        final RedirectView redirectView = new RedirectView("/clients/addClient", true);
        if (!numeroAgence.isEmpty() && (!agenceRepository.existsByNumero(numeroAgence))){
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Aucune agence n'existe avec ce numero: "+numeroAgence);
        }else if (!numeroBanque.isEmpty() && (!banqueRepository.existsByNumero(numeroBanque))){
            redirectAttributes.addFlashAttribute("error", true);
            redirectAttributes.addFlashAttribute("errorMessage", "Aucune banque n'existe avec ce numero: "+numeroBanque);
        }else {
            if (clientRepository.existsByNumero(client.getNumero())){
                redirectAttributes.addFlashAttribute("error", true);
                redirectAttributes.addFlashAttribute("errorMessage", "Un client existe déjà avec ce numero");
            }else {
                String code = String.format("A%dC%010d", Long.parseLong(numeroAgence), currentClientNumero) ;
                currentClientNumero ++;
                client.setNumero(code);
                Client savedClient = clientRepository.save(client);
                createSimpleCompte(client, numeroAgence, numeroBanque);
                redirectAttributes.addFlashAttribute("savedClient", savedClient);
                redirectAttributes.addFlashAttribute("addClientSuccess", true);
            }
        }
        return redirectView;
    }

    public void createSimpleCompte(Client client, String numeroAgence,String numeroBanque) {
        Compte compte = new Compte();
        String code = String.format("A%dCPS%010d", Long.parseLong(numeroAgence), currentCompteNumero) ;
        currentCompteNumero ++;
        compte.setNumero(code);
        compte.setType(TypeCompte.SIMPLE);
        compte.setSolde(0.0);
        compte.setDateOuverture(new Date().getTime());
        compte.setTaux(0.0);
        compte.setNomClient(client.getNom());
        compte.setPrenomClient(client.getPrenom());
        compte.setNumeroAgence(numeroAgence);
        compte.setNumeroBanque(numeroBanque);
        compteRepository.save(compte);
    }

    @GetMapping("/listClientsDecouvert")
    public String getClientsDecouvert(Model model) {
        model.addAttribute("client", clientRepository.getClientsByTypeCompte(TypeCompte.A_DECOUVERT.toString()));
        return "list-clients-decouvert";
    }
}
