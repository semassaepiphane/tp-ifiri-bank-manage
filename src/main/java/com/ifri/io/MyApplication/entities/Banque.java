package com.ifri.io.MyApplication.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Banque {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String numero;
    private double capital;
    private String adresseSiege;
    @ElementCollection
    private List<Integer> numSalaries;
    @ElementCollection
    private List<String> villesAgences;

    public Banque() {
    }

    public Banque(String numero, double capital, String adresseSiege) {
        this.numero = numero;
        this.capital = capital;
        this.adresseSiege = adresseSiege;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public double getCapital() {
        return capital;
    }

    public void setCapital(double capital) {
        this.capital = capital;
    }

    public String getAdresseSiege() {
        return adresseSiege;
    }

    public void setAdresseSiege(String adresseSiege) {
        this.adresseSiege = adresseSiege;
    }

    public List<Integer> getNumSalaries() {
        return numSalaries;
    }

    public void setNumSalaries(List<Integer> numSalaries) {
        this.numSalaries = numSalaries;
    }

    public List<String> getVillesAgences() {
        return villesAgences;
    }

    public void setVillesAgences(List<String> villesAgences) {
        this.villesAgences = villesAgences;
    }
}
