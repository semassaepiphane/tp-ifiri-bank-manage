package com.ifri.io.MyApplication.entities;

import com.ifri.io.MyApplication.entities.enums.TypeCompte;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;

@Entity
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Enumerated(EnumType.STRING)
    private TypeCompte type;
    @Column(unique = true)
    private String numero;
    private double solde;
    private double dateOuverture;
    private double taux;
    private String nomClient;
    private String prenomClient;
    private String numeroAgence;
    private String numeroBanque;

    public Compte() {
    }

    public Compte(TypeCompte type, String numero, double solde, double dateOuverture, String nomClient, String prenomClient,
                  String numeroAgence, String numeroBanque) {
        this.type = type;
        this.numero = numero;
        this.solde = solde;
        this.dateOuverture = dateOuverture;
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.numeroAgence = numeroAgence;
        this.numeroBanque = numeroBanque;
    }

    public Compte(TypeCompte type, String numero, double solde, double dateOuverture, double taux, String nomClient,
                  String prenomClient, String numeroAgence, String numeroBanque) {
        this.type = type;
        this.numero = numero;
        this.solde = solde;
        this.dateOuverture = dateOuverture;
        this.taux = taux;
        this.nomClient = nomClient;
        this.prenomClient = prenomClient;
        this.numeroAgence = numeroAgence;
        this.numeroBanque = numeroBanque;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TypeCompte getType() {
        return type;
    }

    public void setType(TypeCompte type) {
        this.type = type;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public double getDateOuverture() {
        return dateOuverture;
    }

    public void setDateOuverture(double dateOuverture) {
        this.dateOuverture = dateOuverture;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPrenomClient() {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }

    public String getNumeroAgence() {
        return numeroAgence;
    }

    public void setNumeroAgence(String numeroAgence) {
        this.numeroAgence = numeroAgence;
    }

    public String getNumeroBanque() {
        return numeroBanque;
    }

    public void setNumeroBanque(String numeroBanque) {
        this.numeroBanque = numeroBanque;
    }
}
