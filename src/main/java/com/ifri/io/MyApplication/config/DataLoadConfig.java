package com.ifri.io.MyApplication.config;

import com.ifri.io.MyApplication.entities.enums.TypeCompte;
import com.ifri.io.MyApplication.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class DataLoadConfig {

    private final ClientRepository clientRepository;
    private final CompteRepository compteRepository;
    private final EmployeRepository employeRepository;
    private final AgenceRepository agenceRepository;
    private final BanqueRepository banqueRepository;

    @Autowired
    public DataLoadConfig(ClientRepository clientRepository, CompteRepository compteRepository, EmployeRepository employeRepository,
                          AgenceRepository agenceRepository, BanqueRepository banqueRepository) {
        this.clientRepository = clientRepository;
        this.compteRepository = compteRepository;
        this.employeRepository = employeRepository;
        this.agenceRepository = agenceRepository;
        this.banqueRepository = banqueRepository;
    }

    @PostConstruct
    public void init() {
       // System.out.println("\n getClientsByTypeCompte: "+ clientRepository.getClientsByTypeCompte(TypeCompte.A_DECOUVERT.toString()));
    }

    public void loadGlobalData(){

    }
}
